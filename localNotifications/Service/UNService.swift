//
//  UNService.swift //UN Stands for User Notifications
//  localNotifications
//
//  Created by Mario on 7/24/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation
import UserNotifications


class UNService: NSObject{
    
    private override init () {}
    static let shared = UNService()
    let unCenter = UNUserNotificationCenter.current()
    
    
    //MARK:- Configuration Methods
    func authorize()
    {
        let options : UNAuthorizationOptions = [.alert, .badge, .sound, .carPlay]
        unCenter.requestAuthorization(options: options) { (granted, error) in
            if (error != nil)
            {
                print(error ?? "Error in auths")
            }// We should check the handle of the error 🧐
            else
            {
                guard granted else
                {
                    print("USER DENIED ACCESS")
                    return
                }
                self.configure()
            }
        }
    }
    func configure()
    {
        unCenter.delegate = self
        setUpActionsAndCategories()
    }
    
    //MARK:- Triggers Methods
    func timerRequest(with interval: TimeInterval)
    {
        /*
         1) Content (which you send)
         2) Event that triggered
         3) Actual request queue
         */
        
        //1) Content
        let content = UNMutableNotificationContent()
        content.title = "Timer finished"
        content.body = "Your timer is all done!"
        content.sound = .default
        //content.badge = 1
        content.categoryIdentifier = NotificationCategory.timer.rawValue
        if let attachment = getAttachment(for: .timer)
        {
            content.attachments = [attachment]
        }
        //2) Event that trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: false) // The interval requieres 60 seconds to repeat or it will crash
       //3) Actual request queue
        let request = UNNotificationRequest(identifier: "userNotification.timer", content: content, trigger: trigger)
        
        unCenter.add(request, withCompletionHandler: nil)
        
    }
    func dateRequest(with components: DateComponents)
    {
        //1) Content
        let content = UNMutableNotificationContent()
        content.title = "Date triggered"
        content.body = "It is now the future!"
        content.sound = .default
        //content.badge = 1
        content.categoryIdentifier = NotificationCategory.date.rawValue
        
        if let attachment = getAttachment(for: .date)
        {
            content.attachments = [attachment]
        }
        //2) Event that trigger
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
        //3) Actual request queue
        let request = UNNotificationRequest(identifier: "userNotification.date", content: content, trigger: trigger)
        
        
        unCenter.add(request, withCompletionHandler: nil)
    }
    func locationRequest()
    {
        //1) Content
        let content = UNMutableNotificationContent()
        content.title = "You have returned"
        content.body = "Welcome back bitch!"
        content.sound = .default
        //content.badge = 1
        content.categoryIdentifier = NotificationCategory.location.rawValue
        if let attachment = getAttachment(for: .location)
        {
            content.attachments = [attachment]
        }
        //3) Actual request queue
        let request = UNNotificationRequest(identifier: "userNotification.location", content: content, trigger: nil)
        
        unCenter.add(request, withCompletionHandler: nil)
    }
    //MARK:- Configurations and helpers methods
    func setUpActionsAndCategories()
    {
        let timerAction = UNNotificationAction(identifier: NotificationActionID.timer.rawValue,
                                               title: "Run timer logic",
                                               options: [.authenticationRequired])
        let dateAction = UNNotificationAction(identifier: NotificationActionID.date.rawValue,
                                               title: "Run date logic",
                                               options: [.destructive])
        let locationAction = UNNotificationAction(identifier: NotificationActionID.location.rawValue,
                                               title: "Run location logic",
                                               options: [.foreground])
        
        
        let secondTimerAction = UNNotificationAction(identifier: NotificationActionID.timer2.rawValue,
                                                  title: "Run timer 2 logic",
                                                  options: [.foreground])
        
        let timerCategory = UNNotificationCategory(identifier: NotificationCategory.timer.rawValue,
                                                   actions: [timerAction, secondTimerAction],
                                                   intentIdentifiers: [])
        
        let dateCategory = UNNotificationCategory(identifier: NotificationCategory.date.rawValue,
                                                   actions: [dateAction],
                                                   intentIdentifiers: [])
        
        
        let locationCategory = UNNotificationCategory(identifier: NotificationCategory.location.rawValue,
                                                   actions: [locationAction],
                                                   intentIdentifiers: [])
        
        
        unCenter.setNotificationCategories([timerCategory,dateCategory,locationCategory])
        
    }
    
    func getAttachment(for id: NotificationAttachmentID) -> UNNotificationAttachment?
    {
        var imageName: String
        switch id {
        case .timer:
            imageName = "TimeAlert"
            break
            
        case .date:
            imageName = "DateAlert"
            break
            
        case .location:
            imageName = "LocationAlert"
            break
        }
        guard let  url = Bundle.main.url(forResource: imageName, withExtension: "png") else { return nil }
        do{
            let attachment = try UNNotificationAttachment(identifier: id.rawValue, url: url)
            return attachment
        }catch{
            return nil
        }
        
     
    }
}

//MARK:- NotificationCenterDelegate Extension
extension UNService: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("UN Did recieve response")
        
        if let action = NotificationActionID(rawValue: response.actionIdentifier)
        {
            NotificationCenter.default.post(name: NSNotification.Name("internalNotification.handleAction"), object: action)
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("UN will present")
        
        let options: UNNotificationPresentationOptions = [.alert, .sound]
        completionHandler(options)
    }
}
