//
//  AlertService.swift
//  localNotifications
//
//  Created by Mario on 7/24/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class  AlertService: NSObject {
    
    private override init(){}
    
    static func actionSheet(in vc: UIViewController, title: String, completion: @escaping() -> Void)
    {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: title, style: .default) { (_) in
            completion()
        }
        alert.addAction(action)
        vc.present(alert, animated: true)
    }
}
