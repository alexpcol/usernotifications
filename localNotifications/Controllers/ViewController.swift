//
//  ViewController.swift
//  localNotifications
//
//  Created by Mario on 7/24/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - ViewCycleMethods
    override func viewDidLoad() {
        super.viewDidLoad()
        UNService.shared.authorize()
        CLService.shared.authorize()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didEnterRegion),
                                               name: NSNotification.Name("internalNotification.enteredRegion"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleNotificationsActions),
                                               name: NSNotification.Name("internalNotification.handleAction"),
                                               object: nil)
        
    }
    // MARK: - SelectorMethods
    @objc func handleNotificationsActions(_ sender: Notification)
    {
        guard let action = sender.object as? NotificationActionID else {return}
        switch action {
        case .timer:
            print("Timer logic dooo")
            break
        case .timer2:
            print("Timer 2 logic dooo")
            break
        case .date:
            print("Date logic dooo")
            break
        case .location:
            print("Location logic dooo")
            break
        }
    }
    
    @objc func didEnterRegion()
    {
        UNService.shared.locationRequest()
    }
    
    // MARK: - ActionMethods
    @IBAction func timerTapped(_ sender: Any)
    {
        AlertService.actionSheet(in: self, title: "5 Seconds") {
            UNService.shared.timerRequest(with: 10)
        }
        
    }
    
    @IBAction func dateTapped(_ sender: Any)
    {
        AlertService.actionSheet(in: self, title: "Some future time") {
            var compoments = DateComponents()
            compoments.second = 0
            UNService.shared.dateRequest(with: compoments)
        }
        
    }
    
    @IBAction func locationTapped(_ sender: Any)
    {
        AlertService.actionSheet(in: self, title: "When I reach a point") {
            CLService.shared.updateLocation()
        }
    }
    

}

