//
//  NotificationActionID.swift
//  localNotifications
//
//  Created by Mario on 7/24/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation


enum NotificationActionID: String{
    case timer = "userNotification.action.timer"
    case timer2 = "userNotification.action.timer2"
    case date = "userNotification.action.date"
    case location = "userNotification.action.location"
}

