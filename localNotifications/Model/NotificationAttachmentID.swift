//
//  NotificationAttachmentID.swift
//  localNotifications
//
//  Created by Mario on 7/24/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation

enum NotificationAttachmentID : String
{
    case timer = "userNotification.attachment.timer"
    case date = "userNotification.attachment.date"
    case location = "userNotification.attachment.location"
}
