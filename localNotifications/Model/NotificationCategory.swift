//
//  NotificationCategory.swift
//  localNotifications
//
//  Created by Mario on 7/24/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation


enum NotificationCategory: String {
    case timer = "userNotification.category.timer"
    case date = "userNotification.category.date"
    case location = "userNotification.category.location"
}
